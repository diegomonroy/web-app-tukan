<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Location_countryRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class Location_countryCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class Location_countryCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ShowOperation;

    public function setup()
    {
        $this->crud->setModel('App\Models\Location_country');
        $this->crud->setRoute(config('backpack.base.route_prefix') . '/location_country');
        $this->crud->setEntityNameStrings(trans('general.location_country'), trans('general.location_countries'));
    }

    protected function setupListOperation()
    {
        // TODO: remove setFromDb() and manually define Columns, maybe Filters
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------------
		| FUNCTIONS
		|--------------------------------------------------------------------------------
		*/

		$user = backpack_user();

		/*
		|--------------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------------
		*/

		/* ID */

		$this->crud->addColumn([
			'name' => 'id',
			'label' => trans('general.id'),
		]);

		/* País */

		$this->crud->addColumn([
			'name' => 'name',
			'label' => trans('general.country'),
			'limit' => 255,
		]);

		/* Código */

		$this->crud->addColumn([
			'name' => 'code',
			'label' => trans('general.code'),
			'limit' => 255,
		]);

		/*
		|--------------------------------------------------------------------------
		| TABLE
		|--------------------------------------------------------------------------
		*/

		$this->crud->enableExportButtons();

    }

    protected function setupCreateOperation()
    {
        $this->crud->setValidation(Location_countryRequest::class);

        // TODO: remove setFromDb() and manually define Fields
        // $this->crud->setFromDb();

		/*
		|--------------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------------
		*/

		/* País */

		$this->crud->addField([
			'name' => 'name',
			'label' => trans('general.country'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Código */

		$this->crud->addField([
			'name' => 'code',
			'label' => trans('general.code'),
			'type' => 'text',
			'wrapperAttributes' => [
				'class' => 'form-group col-md-12'
			],
		]);

		/* Created By */

		$this->crud->addField([
			'name' => 'created_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'create');

    }

    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();

		/*
		|--------------------------------------------------------------------------------
		| CRUD
		|--------------------------------------------------------------------------------
		*/

		/* Updated By */

		$this->crud->addField([
			'name' => 'updated_by',
			'type' => 'hidden',
			'value' => backpack_user()->id,
		], 'update');

    }
}
