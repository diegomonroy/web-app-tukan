<!-- This file is used to store sidebar items, starting with Backpack\Base 0.9.0 -->
<!-- Begin Dashboard -->
	<li class="nav-item"><a href="{{ backpack_url('dashboard') }}" class="nav-link"><i class="fa fa-dashboard nav-icon" aria-hidden="true"></i> <span>{{ trans('general.dashboard') }}</span></a></li>
<!-- End Dashboard -->
<!-- Begin Tukán -->
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-circle nav-icon" aria-hidden="true"></i> <span>{{ trans('general.tukan') }}</span></a>
		<ul class="nav-dropdown-items">
		</ul>
	</li>
<!-- End Tukán -->
<!-- Begin Dropdowns -->
	@can('Ver Desplegables')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-caret-square-o-down nav-icon" aria-hidden="true"></i> <span>{{ trans('general.dropdowns') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('location_country') }}" class="nav-link"><i class="fa fa-sort-desc nav-icon" aria-hidden="true"></i> <span>{{ trans('general.location_countries') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Dropdowns -->
<!-- Begin CMS -->
	@can('Ver CMS')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-chrome nav-icon" aria-hidden="true"></i> <span>{{ trans('general.cms') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('page') }}" class="nav-link"><i class="fa fa-file-o nav-icon" aria-hidden="true"></i> <span>{{ trans('general.pages') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('menu-item') }}" class="nav-link"><i class="fa fa-list nav-icon" aria-hidden="true"></i> <span>{{ trans('general.menus') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End CMS -->
<!-- Begin News -->
	@can('Ver Noticias')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-newspaper-o nav-icon" aria-hidden="true"></i> <span>{{ trans('general.news') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('article') }}" class="nav-link"><i class="fa fa-newspaper-o nav-icon" aria-hidden="true"></i> <span>{{ trans('general.articles') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('category') }}" class="nav-link"><i class="fa fa-list nav-icon" aria-hidden="true"></i> <span>{{ trans('general.categories') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('tag') }}" class="nav-link"><i class="fa fa-tag nav-icon" aria-hidden="true"></i> <span>{{ trans('general.tags') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End News -->
<!-- Begin Files -->
	@can('Ver Archivos')
	<li class="nav-item"><a href="{{ backpack_url('elfinder') }}" class="nav-link"><i class="fa fa-files-o nav-icon" aria-hidden="true"></i> <span>{{ trans('general.files') }}</span></a></li>
	@endcan
<!-- End Files -->
<!-- Begin Translations -->
	@can('Ver Traducciones')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-globe nav-icon" aria-hidden="true"></i> <span>{{ trans('general.translations') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('language') }}" class="nav-link"><i class="fa fa-flag-checkered nav-icon" aria-hidden="true"></i> <span>{{ trans('general.languages') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('language/texts') }}" class="nav-link"><i class="nav-icon fa fa-language" aria-hidden="true"></i> <span>{{ trans('general.language_files') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Translations -->
<!-- Begin Users -->
	@can('Ver Usuarios')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-group nav-icon" aria-hidden="true"></i> <span>{{ trans('general.authentication') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('user') }}" class="nav-link"><i class="fa fa-user nav-icon" aria-hidden="true"></i> <span>{{ trans('general.users') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('role') }}" class="nav-link"><i class="fa fa-group nav-icon" aria-hidden="true"></i> <span>{{ trans('general.roles') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('permission') }}" class="nav-link"><i class="fa fa-key nav-icon" aria-hidden="true"></i> <span>{{ trans('general.permissions') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Users -->
<!-- Begin Configuration -->
	@can('Ver Configuración')
	<li class="nav-item nav-dropdown">
		<a href="#" class="nav-link nav-dropdown-toggle"><i class="fa fa-cogs nav-icon" aria-hidden="true"></i> <span>{{ trans('general.configuration') }}</span></a>
		<ul class="nav-dropdown-items">
			<li class="nav-item"><a href="{{ backpack_url('setting') }}" class="nav-link"><i class="fa fa-cog nav-icon" aria-hidden="true"></i> <span>{{ trans('general.settings') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('backup') }}" class="nav-link"><i class="fa fa-hdd-o nav-icon" aria-hidden="true"></i> <span>{{ trans('general.backups') }}</span></a></li>
			<li class="nav-item"><a href="{{ backpack_url('log') }}" class="nav-link"><i class="fa fa-terminal nav-icon" aria-hidden="true"></i> <span>{{ trans('general.logs') }}</span></a></li>
		</ul>
	</li>
	@endcan
<!-- End Configuration -->